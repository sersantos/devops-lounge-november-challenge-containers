CREATE DATABASE wpdb;

CREATE USER 'wpuser'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON wpdb.* TO 'wpuser'@'localhost';